# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# Configuration for building Python 2.7 if it's not available on the
# build machine.
#

# The name of the package:
atlas_subdir( Python )

# In release rebuild mode stop here:
if( ATLAS_RELEASE_MODE )
   return()
endif()

# Stop if the build is not needed:
if( NOT ATLAS_BUILD_PYTHON )
   return()
endif()

# Tell the user what's happening:
message( STATUS "Building Python as part of this project" )

# The source code for Python:
set( _source "http://cern.ch/lcgpackages/tarFiles/sources/Python-2.7.13.tgz" )
set( _md5 "17add4bf0ad0ec2f08e0cae6d205c700" )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PythonBuild )

# Extra environment options for the build:
set( _extraEnv
   CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER} )
if( NOT APPLE )
   list( APPEND _extraEnv LDFLAGS=-Wl,-rpath,'$$ORIGIN/../lib' )
endif()

# Create the script that will sanitize python-config after the build:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeConfig.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh @ONLY )

# Set up the build of Python in the build directory:
ExternalProject_Add( Python
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   URL ${_source}
   URL_MD5 ${_md5}
   BUILD_IN_SOURCE 1
   CONFIGURE_COMMAND ${CMAKE_COMMAND} -E env ${_extraEnv}
   <SOURCE_DIR>/configure --prefix ${_buildDir} --enable-shared
   --enable-unicode=ucs4 --enable-ipv6
   BUILD_COMMAND make
   INSTALL_COMMAND make install
   COMMAND ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR> )
add_dependencies( Package_Python Python )

# Install Python:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Clean up:
unset( _source )
unset( _md5 )
unset( _buildDir )
unset( _extraEnv )
