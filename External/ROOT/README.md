ROOT - Data Analysis Framework
==============================

This package is used to build ROOT for the offline / analysis release.

The package only builds ROOT if the `ATLAS_BUILD_ROOT` (cache)
variable is set to `TRUE`. It's the responsibility of the project
building this "package" to set a correct value for that variable,
allowing the user to override it from the command line if necessary.
